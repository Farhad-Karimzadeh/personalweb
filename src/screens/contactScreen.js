import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography, TextField, Button } from "@material-ui/core";
import { getTranslate } from "../localization/index";
import Title from "../components/Title";
import "./contact.css"
import Contact from '../components/contact'
import CallIcon from '@material-ui/icons/CallOutlined'
import MailOutlinedIcon from '@material-ui/icons/MailOutlined'
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined'


const useStyle = makeStyles((theme) => ({
  root: {
    minHeight: "100%",
  },
  contactGrid: {
    paddingTop: 70,
    paddingBottom: 60,
    paddingRight: 30,
    paddingLeft: 30,
    [theme.breakpoints.down("xs")]: {
      paddingRight: 5,
      paddingLeft: 5,
    },
  },
  formGrid: {
padding:10,
  },

  detailsGrid: {
   padding:10,
   
  },
  getintouch: {
    textAlign: "left",
  },
  btnSendMeFrm:{
    marginTop:25,
    display:"flex",
  },
  iconStyle:{
      fontSize:30,
    

   
  
   
  },
 
}));

export default function ContactScreen() {
  const translate = getTranslate();
  const classes = useStyle();
  return (
    <Grid container alignItems="flex-start" className={classes.root}>
      <Grid container item xs={12} className={classes.contactGrid}>
        <Title title={translate.contactMe} />
        <Grid container direction="row">
          <Grid item xs={12} md={6} className={classes.formGrid}>
            <Typography className={classes.getintouch} variant="h4">
              {" "}
              {translate.headFormContact}
            </Typography>
            <TextField 
            size="medium"
            style={{marginTop:25}}
            fullWidth
              required
              label={translate.txtFilName}
              variant="outlined"
            />
            <TextField 
            size="medium"
            style={{marginTop:25}}
            fullWidth
              required
              label={translate.txtFilEmail}
              variant="outlined"
            />
            <TextField 
            size="medium"
            style={{marginTop:25}}
            fullWidth
              required
              label={translate.txtFilSubject}
              variant="outlined"
            />
            <TextField 
            size="medium"
            style={{marginTop:25}}
            fullWidth
              required
              label={translate.txtFilMessage}
              variant="outlined"
              multiline
              rows={5}
            />
            <Button variant="contained" color="primary" size="large" className={classes.btnSendMeFrm} >{translate.sendmeForm}</Button>

          </Grid>
          <Grid item xs={12} md={6} className={classes.detailsGrid}>
              <Contact title={translate.frmTitle} text1={translate.frmtxt1} text2={translate.frmtxt2} icon={<CallIcon className={classes.iconStyle} />} />
              <Contact title={translate.frmEmail} text1={translate.frmMail1} text2={translate.frmMail2} icon={<MailOutlinedIcon className={classes.iconStyle} />} />
              <Contact title={translate.frmAddress} text1={translate.frmAddLst1} text2={translate.frmAddLst2} icon={<LocationOnOutlinedIcon className={classes.iconStyle} />} />
             
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
