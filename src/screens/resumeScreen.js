import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography } from "@material-ui/core";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";
import LocalLibraryIcon from "@material-ui/icons/LocalLibrary";
import { getTranslate } from "../localization/index";
import Title from "../components/Title";
import Skill from "../components/skill";
import MyStepper from "../components/MyStepper";

const useStyle = makeStyles((theme) => ({
  root: {
    minHeight: "100vh",
  },
  skillGrid: {
    paddingTop: 70,
    paddingBottom: 60,
    paddingRight: 30,
    paddingLeft: 30,
    [theme.breakpoints.down('xs')]:{
      paddingRight: 15,
      paddingLeft: 15,

    }

  },
  skillsecoundtitle:{
  marginLeft: 10,
}
}));

export default function ResumeScreen() {
  const translate = getTranslate();
  const classes = useStyle();
 
  return (
    <Grid container alignItems="flex-start" className={classes.root}>
      <Grid container item xs={12} className={classes.skillGrid}>
        <Title title={translate.skills} />
        <Grid container direction="row">
          <Grid item xs={12} md={6} className={classes.imageGrid}>
            <Skill value={85} title="ReactJS" />
            <Skill value={88} title="HTML" />
            <Skill value={90} title="CSS" />
            <Skill value={95} title="JAVA/Spring" />
            <Skill value={90} title="Python" />
          </Grid>
          <Grid item xs={12} md={6} className={classes.aboutTextGrid}>
            <Skill value={70} title="Django" />
            <Skill value={70} title="Andriod" />
            <Skill value={98} title="CI/CD" />
            <Skill value={95} title="TDD" />
            <Skill value={100} title="Agile" />
          </Grid>
        </Grid>
      </Grid>
      <Grid container item xs={12} className={classes.skillGrid}>
            <Title title={translate.resume} />
             <Grid
               container
               direction="row"
               alignItems="center"
                justify="flex-start"
             >
                     <BusinessCenterIcon style={{ fontSize: 34 }} />
                     {"  "}

                     <Typography className={classes.skillsecoundtitle} variant="h4">
                       {translate.workingExperiance}
                     </Typography>
             </Grid>
             <Grid container style={{ marginTop: 15, marginBottom: 15 }}>
                     <MyStepper steps={translate.workingSteps} />
             </Grid>
      



      <Grid
          container
          direction="row"
          alignItems="center"
          justify="flex-start"
        >
          <LocalLibraryIcon style={{ fontSize: 34 }} />
          {"  "}
          <Typography className={classes.skillsecoundtitle} variant="h4">
            {translate.educationalQualifications}
          </Typography>
        </Grid>
        <Grid container style={{ marginTop: 15, marginBottom: 15 }}>
          <MyStepper steps={translate.educationalSteps}/>
        </Grid>
      </Grid>
      </Grid>
    
  );
}
