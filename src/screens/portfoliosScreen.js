import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {Grid} from '@material-ui/core'
import Title from '../components/Title'
import {getTranslate} from '../localization/index'
import Portfolio from '../components/portfolio'

import MFr from '../assets/images/fr.PNG'
import PPi from '../assets/images/pi.PNG'
import Tr from '../assets/images/tr.jpg'
import Rs from '../assets/images/Rs.jpg'
import Kn from '../assets/images/kn.jpg'
import Bf from '../assets/images/bf.jpg'


const useStyle = makeStyles(theme =>({
    root:{
        minHeight:"100vh",
        padding :30,
        paddingTop:60,
        [theme.breakpoints.down("xs")]:{
            paddingLeft:10,
            paddingRight:10,
        },
    },


}))

export default function PortfoliosScreen() {
    const translate = getTranslate()
 
 
    const classes = useStyle()
    return (
       <div className={classes.root}>
           <Title title={translate.portfolios} />
           <Grid container item xs={12} alignItem ="center" justify="flex-start" direction = "row" >
            <Portfolio image={MFr} title={translate.portMTitle} decs={translate.portMDecs} />
            <Portfolio image={PPi} title={translate.portPTitle} decs={translate.portPDecs} />
            <Portfolio image={Tr} title={translate.portTTitle} decs={translate.portTDecs} />
            <Portfolio image={Rs} title={translate.portRTitle} decs={translate.portRDecs} />
            <Portfolio image={Kn} title={translate.portKTitle} decs={translate.portKDecs} />
            <Portfolio image={Bf} title={translate.portBTitle} decs={translate.portBDecs} />
             
           </Grid>
       </div>
    )
}
