import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import { Grid,Typography,Button} from '@material-ui/core'
import { getTranslate } from '../localization/index'
import Title from '../components/Title'
import MainImage from '../assets/images/myPicture.jpg'
import Service from '../components/Service'
import DeveloperModeSharpIcon from '@material-ui/icons/DeveloperModeSharp'
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn'
import FolderOpenSharpIcon from '@material-ui/icons/FolderOpenSharp'
import {getDirection} from '../localization/index'

const useStyle = makeStyles(theme =>({
  root:{
    minHeight:"100vh",
  
        },
    aboutGrid:{
        // marginTop:60,
            paddingTop:70,
            paddingBottom:60, 
            paddingRight:30,
            paddingLeft:30,
            // position:"absolute"
            
        },
    mainImage:{
        width:"100%",
        height:"100%"
        },
    imageGrid:{
        height:400,
        position:"relative",
        "&::after":{
            content:"''",
            position: "absolute",
            right:0,
            top:"auto",
            bottom:0,
            height: "65%",
            width: 15,
            background: "rgba(3,127,255,.6)",
        },
        "&::before":{
            content:"''",
            position: "absolute",
            left:0,
            top:0,
            height: "65%",
            width: 15,
            background: "rgba(3,127,255,.6)",
        }

    },
    nameText:{
        color:theme.palette.primary.main,
        [theme.breakpoints.down("xs")]:{
            display:"block"
        }},
    aboutTextGrid:{
        paddingLeft:30,
        paddingRight:30,
        [theme.breakpoints.down('xs')]:{
            paddingLeft:0,
        paddingRight:0,
        marginTop:15,
        marginButton:15,
        }
    },
    btnCVStyle:{
        display:'flex',
        marginTop:30,
        [theme.breakpoints.down('xs')]:{
            alignItems:"center!important",
            justify:"center!important",
            display:"null!important"
        }
    },
    iconStyle:{
        fontSize:46,
        color:theme.palette.primary.main,
    },

    descStyle:{
        minWidth:100,
        display:"inline-block"
    },
    mainDesc:{
        marginTop:32
    }


}))


export default function AboutScreen() {
    const translate = getTranslate()
    const classes = useStyle()
    return (
        <Grid container alignItems="flex-start" className={classes.root}>
            <Grid container item xs={12} className={classes.aboutGrid}>
                <Title title ={translate.aboutMe}/>
                <Grid container direction="row" >
                    <Grid item xs={12} md={6} className={classes.imageGrid} > 
                         <img className={classes.mainImage} src={MainImage} alt={translate.name} />
                    </Grid>
                    <Grid item xs={12} md={6} className={classes.aboutTextGrid} >
                         <Typography variant="h3">{translate.hiWithName} <span className={classes.nameText}> {translate.name} </span>{translate.hiWithName2} </Typography>
                         <Typography variant="body1" style={{marginTop:40}}>{translate.desc}</Typography>
                         <Typography variant="body1" className={classes.mainDesc} >
<b className={classes.descStyle} > {translate.fullName} </b>
{ ' : ' }
{translate.name}
                         </Typography>
                         <Typography variant="body1" style={{marginTop:5}} >
<b className={classes.descStyle} >{translate.age}</b>
{ ' : ' }
{translate.answerAge}
                         </Typography>
                         <Typography variant="body1" style={{marginTop:5}} >
<b className={classes.descStyle} >{translate.nationality}</b>
{ ' : ' }
{translate.answerNationality}
                         </Typography>
                         <Typography variant="body1" style={{marginTop:5}} >
<b className={classes.descStyle} >{translate.language}</b>
{ ' : ' }
{translate.answerLanguage}
                         </Typography>
                         {/* <Typography variant="body1" style={{marginTop:5}} >
<b className={classes.descStyle} >{translate.address}</b>
{ ' : ' }
{translate.answerAddress}
                         </Typography> */}
                         <Typography variant="body1" style={{marginTop:5}} >
<b className={classes.descStyle} >{translate.phone}</b>
{ ' : ' }
{translate.answerPhone}
                         </Typography>
                         <div className={classes.btnCVStyle}>
                         <Button  variant="contained" color="primary">{translate.dlCVBTN}</Button>
                      

                         </div>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container item xs={12} className={classes.aboutGrid}>
                <Title title ={translate.services}/>
                <Grid container direction="row" >

                    <Service icon={<DeveloperModeSharpIcon className={classes.iconStyle} />} title={translate.serviceFstTitle} desc={translate.serviceFstDesc} />
                    <Service icon={<MonetizationOnIcon className={classes.iconStyle} />} title={translate.serviceSndTitle} desc={translate.serviceSndDesc} />
                    <Service icon={<FolderOpenSharpIcon className={classes.iconStyle} />} title={translate.serviceTrdTitle} desc={translate.serviceTrdDesc} />
                    
                </Grid>
            </Grid>
        </Grid>
    )
}
