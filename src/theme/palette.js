//color
export default{
    primary:{
        main:"#037fff",
        contrastText:"#FFF",
        background:"#191d2b"
    },
    secondary:{
        main:"#a4acc4",

    },
    text:{
        primary: "#fff",
        secondary:"#a4acc4"
    },
}