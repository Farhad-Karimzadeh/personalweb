import React from 'react'
import {makeStyles} from '@material-ui/core/styles'
import { Grid, Typography } from '@material-ui/core'


const useStyles=makeStyles(theme=>({
    root:{
        width:"100%",
        marginTop:20,
    },
    main:{
       
        backgroundColor:"#191d2b",
        zIndex:1,
        padding:theme.spacing(8),
        paddingLeft:0,
        border:"1px solid #2e344e",
        borderTop:"5px solid #2e344e",
        transition:"1s",
        "&:hover":{
            borderTopColor:theme.palette.primary.main,
            transition:"1s",

        }
    },

    title:{
        textAlign:"left",
       },
    desc:{
     cursor:"pointer",
     transition:".4s",
     "&:hover":{
         color:theme.palette.primary.main,
         transition:".4s",
     }
    },
    divIcon:{
        margin:10,
        padding:10,
        border:"1px solid #2e344e",
        paddingBottom:5,
        marginRight:25,
        marginLeft:25,
        
    },
    
}))

export default function Contact({title , text1 , text2 ,icon}) {
    const classes = useStyles()
    return (
        <div item lg={4} md={6} xs={12} className={classes.root}>
           <Grid className={classes.main} container direction="row" alignItems='center' justify="flex-start">
               <div className={classes.divIcon} >
                    {icon}
               </div>
                 
                 <div>
                <Typography className= {classes.title} variant ="h6">{title}</Typography>
                 <Typography className={classes.desc} variant="body1" > {text1}</Typography>
                 <Typography className={classes.desc} variant="body1" > {text2}</Typography>
                 </div>
           </Grid>
           
        </div>
    )
}
