import React,{useState} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {Grid, Typography,Modal} from '@material-ui/core'
import transitions from '@material-ui/core/styles/transitions'

const useStyle = makeStyles(theme => ({
    root:{
        padding:15,
        flexDirection:"column",
        direction:"flex", 
        marginBottom:60,
    },
    image:{
        height:"100%",
        // width:300
    width:"100%",
    cursor:"pointer",
    "&:hover":{
        transform:"rotate(360deg)",
        transition:"1s",
    }
    
    },
    title:{
        cursor:"pointer",
        textAlign:"left",
        
        "& a":{
            color:"#FFF",
            transition:".4s",
            textDecoration:"none",
            "&:hover":{
                color:theme.palette.primary.main,
                transition:".4s",
            }
        }
    },
    modalImage:{
        width:'100%',
        height:'70%',
    },
    modal:{
        width:"60%",
        height:'80%',
        marginLeft:"auto",
        marginRight:"auto",
        marginTop:120,
        [theme.breakpoints.down('xs')]:{
            width:"90%",
            height:"50%",
        }
    }
}))

export default function Portfolio({image,title,decs,href}) {
    const classes = useStyle()
    const [open,setOpen]=useState(false)
    return (
        <>
        <Grid item lg={4} className={classes.root}>
         

            <img
            onClick={()=>setOpen(true)} 
            src={ image } alt={title} className={classes.image}/>
            <Typography className={classes.title} variant = "h5"><a href={href}>{title}</a> </Typography>
            <Typography variant = "body1">{decs}</Typography>
     
        </Grid>
        <Modal className={classes.modal} open={open} onClose={()=>setOpen(false)} >
        <img src={ image } alt={title} className={classes.modalImage}/>

        </Modal>
        </>
    )
}
