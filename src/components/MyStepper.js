import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import { StepConnector} from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  stepConnector:{
    paddingBottom:0,
    "& span":{
      borderLeft:"3px solid #2e344e"
    },
      },
  stepContent:{
        marginTop:0,
        borderLeft:"3px solid #2e344e"
      },
  stepColorCircle:{
        width:16,
        height:16,
        borderRadius:8,
        border:"5px solid #2e344e",
        marginLeft:"5px",

      },
  label:{
        display:"flex",
        flexDirection:"row",
        alignItems:'center',
        justifyContent:'flex-start',
        [theme.breakpoints.down('xs')]:{
          justifyContent:'center',
          flexDirection:"column",
          alignItems:"left",
          display:"table-cell"
        }
      },
  title:{

        marginLeft:10,
        color:theme.palette.primary.main,
      },
  line:{
    width:30,
    height:1,
    backgroundColor:"#2e344e",
  },
  date:{
    width:200,
    textAlign:"left"
  },
  divTitle:{
    marginLeft:235,
    [theme.breakpoints.down('xs')]:{
        marginLeft:8,
        justifyContent:'center',
        flexDirection:"column",
        alignItems:"flex-start"
    }
  },
  subtitlestep:{
    display:"flex",
    marginBottom:10,
    marginTop:15,
    textAlign:"left"
     
  },
  stepper:{
    backgroundColor:"transparent",
    [theme.breakpoints.down('xs')]:{
      marginLeft:0,
      paddingLeft:0,
      paddingRight:0,
    }
  }
}))

export default function MyStepper({steps }) {
  const classes = useStyles();



  return (
    <div className={classes.root}>
      <Stepper connector={<StepConnector className={classes.stepConnector} />} className={classes.stepper} orientation="vertical">
        {steps.map((step) => step.id>=0 ?(
          <Step active={true} key={step.id}>
            <StepLabel 
            classes={{label:classes.label}}
            className={classes.stepLabel} icon={<span className={classes.stepColorCircle} />} >
                <Typography className={classes.date} variant="h6">{step.date} </Typography>
                <Hidden xsDown>

                <span className={classes.line}></span>
                </Hidden>
                <Typography className={classes.title} variant="h5" > {step.title} </Typography>


            </StepLabel>

    <StepContent className={classes.stepContent} >
      <div className={classes.divTitle}>
             <Typography className={classes.subtitlestep} variant="h6">{step.subTitle}</Typography>
              <Typography variant="body1" > {step.content} </Typography>
             
              </div>
            </StepContent>
          </Step>
        ): step.id === -1 ? (
          <Step active={true} key={step.id}>         
          <StepLabel icon={ null } ></StepLabel>
        </Step>
        ):(null) )}
      </Stepper>
    </div>
  );
}
